package top.wugy.cloud.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

/**
 * guangyao.wu 2019/9/23 9:08
 */
@SpringBootApplication
public class GatewaySimpleApp {

    public static void main(String[] args) {
        SpringApplication.run(GatewaySimpleApp.class, args);
    }

    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder) {
        return builder.routes().route(r -> r.path("/jd").uri("http://jd.com").id("jd_route")).build();
    }
}
