package top.wugy.cloud.hystrix.simple.service;

import org.springframework.stereotype.Service;

@Service
public class UserServiceFallback implements IUserService {

    /**
     * 出错则调用该方法返回友好错误
     */
    public String getUser(String username) {
        return "The user does not exist in this system, please confirm username";
    }
}