package top.wugy.cloud.hystrix.simple.service;

/**
 * guangyao.wu 2019/8/7 7:48
 */
public interface IUserService {

    String getUser(String username);
}
