package top.wugy.cloud.hystrix.simple.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import top.wugy.cloud.hystrix.simple.service.IUserService;

/**
 * guangyao.wu 2019/8/7 7:52
 */
@RestController
public class TestController {

    @Autowired
    private IUserService userService;

    @GetMapping("/getUser")
    public String getUser(@RequestParam String username) {
        return userService.getUser(username);
    }
}
