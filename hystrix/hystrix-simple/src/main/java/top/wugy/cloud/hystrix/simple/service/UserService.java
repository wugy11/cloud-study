//package top.wugy.cloud.hystrix.simple.service;
//
//import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
//import org.springframework.stereotype.Service;
//
///**
// * guangyao.wu 2019/8/7 7:49
// */
//@Service
//public class UserService implements IUserService {
//
//    @Override
//    @HystrixCommand(fallbackMethod = "defaultUser")
//    public String getUser(String username) {
//        if ("spring".equalsIgnoreCase(username))
//            return "This is real user";
//        throw new RuntimeException(username + " is error!");
//    }
//
//    public String defaultUser(String username) {
//        return "The user does not exist in this system";
//    }
//
//}
