package top.wugy.cloud.hystrix.exception.service;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;

/**
 * guangyao.wu 2019/8/10 19:22
 */
public class ProviderServiceCommand extends HystrixCommand<String> {

    protected ProviderServiceCommand() {
        super(HystrixCommandGroupKey.Factory.asKey("GroupSC"));
    }

    @Override
    protected String run() throws Exception {
        return "Spring Cloud";
    }

    @Override
    protected String getFallback() {
        return "Failure Spring Cloud";
    }
}
