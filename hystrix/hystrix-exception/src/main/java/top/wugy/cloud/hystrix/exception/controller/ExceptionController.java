package top.wugy.cloud.hystrix.exception.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * guangyao.wu 2019/8/10 19:19
 */
@RestController
public class ExceptionController {

    @GetMapping("/getFallbackMethodTest")
    @HystrixCommand
    public String getFallbackMethodTest(String id){
        throw new RuntimeException("getFallbackMethodTest failed");
    }

    public String fallback(String id, Throwable throwable) {
        System.out.println(throwable.getMessage());
        return "this is fallback message";
    }
}
