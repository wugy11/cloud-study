package top.wugy.cloud.hystrix.exception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * guangyao.wu 2019/8/10 19:17
 */
@SpringBootApplication
@EnableFeignClients
@EnableHystrix
@EnableDiscoveryClient
public class HystrixExceptionApp {

    public static void main(String[] args) {
        SpringApplication.run(HystrixExceptionApp.class, args);
    }
}
