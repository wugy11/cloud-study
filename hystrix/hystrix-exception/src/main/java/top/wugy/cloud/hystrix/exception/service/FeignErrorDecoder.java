package top.wugy.cloud.hystrix.exception.service;

import com.netflix.hystrix.exception.HystrixBadRequestException;
import feign.FeignException;
import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;
import org.springframework.stereotype.Component;

/**
 * guangyao.wu 2019/8/10 19:34
 */
@Component
public class FeignErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {
        int status = response.status();
        try {
            if (status >= 400 && status <= 499) {
                throw new HystrixBadRequestException(Util.toString(response.body().asReader()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FeignException.errorStatus(methodKey, response);
    }
}
